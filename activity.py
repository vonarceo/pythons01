name = "Von"
age = 31
occupation = "SE/WD"
movie = "Avatar"
rating = 99.6

print(f"I am {name}, and I am {age}, I work as a {occupation}, and my rating for {movie} is {rating}")

num1, num2, num3 = 50, 100, 150

print(num1 * num2)
print(num1 < num3)

num2 += num3

print(num2)
